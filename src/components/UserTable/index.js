import React, { Component } from 'react';
import PropTypes from 'prop-types';

class UserTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [{
                name: {
                    first: "",
                    last: ""
                }
            }]
        }
    }

    componentDidMount() {
        fetch('https://randomuser.me/api/?results=1000')
            .then(results => results.json())
            .then(data => {
                this.setState(prevState => ({
                    users: data.results
                }))
            });

        console.log(this.state.users);
    }


    render() {
        console.log(this.state)
        return (
            <table className="table table-border">

                <tbody>
                    <tr>
                    <th>Name</th>
                    <th>Phone</th>
                </tr>
                    {
                        this.state.users.map((user, idx) => (
                            <tr key ={idx}>
                                <td>{user.name.first + " " + user.name.last}</td>
                                <td>{user.phone}</td>
                            </tr>))
                    }
                </tbody>
            </table>
        );
    }
}

UserTable.propTypes = {

};

const mapStateToProps = state => ({

})

export default UserTable;