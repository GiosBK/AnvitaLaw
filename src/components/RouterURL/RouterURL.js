import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from '../Login/Login';
import Home from '../Home/Home';
import UsersManager from '../UsersManager/UsersManager';
import LawManager from '../LawMangager/LawManager';
import AddUser from '../UsersManager/AddUser';

class RouterURL extends Component {
    render() {
        return (
          
               <div>
                   <Route exact path="/login" component={Login}/>
                   <Route path="/lawsManager" component={LawManager}/>
                   <Route path="/usersManager" component={UsersManager}/>
                   <Route exact path="/home" component={Home}/>
                   <Route path="/add" component={AddUser}/>
                </div>
           
        );
    }
}

export default RouterURL;