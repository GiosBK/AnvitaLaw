import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div className="container-fluid">
                    <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button>
                    <a className="navbar-brand" href="#"><span>QUẢN TRỊ LUẬT</span></a>
                    <ul className="user-menu">
                        <li className="dropdown pull-right">
                        <a href="#" className="dropdown-toggle" data-toggle="dropdown"><svg className="glyph stroked male-user"><use xlinkHref="#stroked-male-user" /></svg><span style={{color: 'white'}}>Xin chào, Thành viên</span> <span className="caret" /></a>
                        </li>
                    </ul>
                    </div>
                </div>
                 </nav>
            </div>
        );
    }
}

export default Header;