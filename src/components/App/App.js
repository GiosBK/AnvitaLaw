import React, { Component } from 'react';
import Nav from '../Nav/Nav';
import Home from '../Home/Home';
import Content from '../Content/Content';
import Login from '../Login/Login';
import RouterURL from '../RouterURL/RouterURL';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <Router>
      <div>
        <div className="container">
          <RouterURL></RouterURL>
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
