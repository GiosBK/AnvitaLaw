import React, { Component } from 'react';
import Header from '../Header/Header';
import Nav from '../Nav/Nav';
import UserTable from '../UserTable';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class UsersManager extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [{
                id: "",
                name: "",
                email: "",
                avatar: "",
                phoneNumber: ""
            }]
        }
    }
    componentDidMount() {
        fetch('http://vnas.itrc.hanu.vn:2001/swagger-ui/#/usermanagerapi/usermanagerapi#getall')
            .then(results => results.json())
            .then(data => {
                this.setState(prevState => ({
                    users: data.results
                }))
            });

        console.log(this.state.users);
    }

    render() {
        return (
            <div>
                <Header></Header>
                <div className="row">
                    <Nav></Nav>
                    {/* <UserTable></UserTable> */}
                  
                    <div className="panel panel-default ctn-users ">					
                        <div className="panel-body">
                            <Link to="/add" className="btn btn-primary">Thêm thành viên mới</Link>	
                            <form role="search">
                                <div className="sr-inline">
                                    <input type="text" className="form-control" placeholder="Search by name" />
                                </div>
                            </form>			
                            <table className= "tb-users table table-border" data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-sort-name="name" data-sort-order="desc">
                            <thead>
                                <tr>						        
                                <th data-sortable="true">ID</th>
                                <th data-sortable="true">Name</th>
                                <th data-sortable="true">Email</th>
                                <th data-sortable="true">PhoneNumber</th>
                                <th data-sortable="true">Avatar</th>
                                <th data-sortable="true">Edit</th>
                                <th data-sortable="true">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style={{height: 300}}>
                                <td data-checkbox="true">1</td>
                                <td data-checkbox="true"><a href="#">GiosBK</a></td>
                                <td data-checkbox="true">giosbk@gmail.com</td>
                                <td data-sortable="true">111</td>
                                <td data-sortable="true">
                                    <span className="thumb"><img width="80px" height="150px" src="anh/prd07.png"  style={{width: 20, height: 20}} /></span>
                                </td>						        
                                <td>
                                    <a href="'edit/id='+{user.id}"><span><img src="./public/image/edit.png" style={{width: 20, height: 20}}/></span></a>
                                </td>
                                <td>
                                    <a href=""><span><img src="./public/image/delete.png" style={{width: 20, height: 20}}/></span></a>
                                </td>
                                </tr>
                                {
                                    this.state.users.map((user,idx)=>(
                                        <tr key ={idx}>
                                            <td data-checkbox="true">{user.id}</td>
                                            <td data-checkbox="true"><a href="#">{user.name}</a></td>
                                            <td data-checkbox="true">{user.email}</td>
                                            <td data-sortable="true">{user.phoneNumber}</td>
                                            <td data-sortable="true">
                                                <span className="thumb"><img width="80px" height="150px" src="anh/prd07.png"  style={{width: 20, height: 20}} /></span>
                                            </td>						        
                                            <td>
                                                <a href="'edit/id='+{user.id}"><span><img src="./public/image/edit.png" style={{width: 20, height: 20}}/></span></a>
                                            </td>
                                            <td>
                                                <a href=""><span><img src="./public/image/delete.png" style={{width: 20, height: 20}}/></span></a>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                            </table>
                            </div>  
                         </div>
                    </div>
            </div>
        );
    }
}

export default UsersManager;