import React, { Component } from 'react';
import Header from '../Header/Header';
import Nav from '../Nav/Nav';

class AddUser extends Component {
    render() {
        return (
            <div>
                <Header></Header>
                <div className="row">
                <Nav></Nav>
                <div className="form-addU">
                <form role="search">
               
                </form>
                <form method="post" encType="multipart/form-data" role="form">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Name</label>
                            <input type="text" className="form-control" name="name" required />
                        </div>
                        <div className="form-group">
                            <label>Email</label>
                            <input type="text" className="form-control" name="email" required />
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input type="text" className="form-control" name="pass"  required />
                        </div>
                        <div className="form-group">
                            <label>PhoneNumber</label>
                            <input type="text" className="form-control" name="phone" required />
                        </div>
                        <div className="form-group">
                            <label>Avatar</label>
                            <input type="file" name="anh_sp" />
                        </div>
                        <div className="btn-sbm">
                            <button type="submit" className="btn btn-primary" name="submit">Update</button>
                        </div>
                    </div>
                    
                </form>
                </div>

                </div>  
            </div>
        );
    }
}

export default AddUser;