import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class Nav extends Component {
    render() {
        return (
            <div id="sidebar-collapse" className="col-sm-3 col-lg-2 sidebar">
                <form role="search">
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Tìm kiếm" />
                </div>
                </form>
                <ul className="nav menu">
               
                    <div className="nav menu">
                        <li className="active"><Link to="/lawsManager" href="#"><svg className="glyph stroked dashboard-dial"><use xlinkHref="#stroked-dashboard-dial" /></svg> Trang chủ quản trị Luật</Link></li>
                        <li className="parent ">
                            <Link to="/usersManager">
                            <span data-toggle="collapse" href="#sub-item-1"><svg className="glyph stroked chevron-down"><use xlinkHref="#stroked-chevron-down" /></svg></span> Quản lí người dùng
                                </Link>
                        </li>
                        <li role="presentation" className="divider" />
                        <li><Link to="/login" href="#"><svg className="glyph stroked male-user"><use xlinkHref="#stroked-male-user" /></svg> Đăng xuất</Link></li>
                    </div>
               
                </ul>
                
          </div>
        );
    }
}

export default Nav;